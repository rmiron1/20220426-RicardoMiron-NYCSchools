# Programming-challenge

## App Requirements
** iOS Coding Challenge: NYC Schools**


**REQUIREMENTS** :

These app requirements are rather high-level and vague. If details are omitted, it is because we will be happy with any of a wide variety of functional solutions. Feel free to be creative and impress with code and UX.

Create a native app to provide information on NYC High schools.

1. Display a list of NYC High Schools.
1. Get your data here: [https://data.cityofnewyork.us/Education/DOE-High-School-Directory-2017/s3k6-pzi2](https://data.cityofnewyork.us/Education/DOE-High-School-Directory-2017/s3k6-pzi2)
2. Selecting a school should show additional information about the school
1. Display all the SAT scores - include Math, Reading and Writing.
1. SAT data here: [https://data.cityofnewyork.us/Education/SAT-Results/f9bf-2cp4](https://data.cityofnewyork.us/Education/SAT-Results/f9bf-2cp4)
2. It is up to you to decide what additional information to display

In order to prevent you from running down rabbit holes that are less important to us, try to prioritize the following:

**What is Important**

- Meet the basic App requirements.
- The App should work like it would do in production - error handling, edge conditions, stability and speed is important.
- Well constructed, easy-to-follow, commented code (especially comment hacks or workarounds made in the interest of expediency (i.e. // given more time I would prefer to wrap this in a blah blah blah pattern blah blah )).
- Proper architecture with of separation of concerns and best-practice coding patterns.
- Modern asynchrounous programming techniques.
- Use Swift as the primary language.
- Be sure the app is compatible with iPhone X

**What is Less Important**

- Demonstrating technologies or techniques you are not already familiar with.
- Only add code which you want us to read and judge you.

**Bonus Points!**

- Good use of UX and UI
- Samples of Unit Testing
- Additional functionality – whatever you see fit.


## Notes
The app was tested on the simulator with various screen sizes and an iPad.
