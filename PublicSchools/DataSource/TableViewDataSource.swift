//
//  SchoolDataSource.swift
//  PublicSchools
//
//  Created by Ricardo Miron on 4/19/22.
//

import Foundation
import UIKit


class TableViewDataSource<T>: NSObject, UITableViewDataSource {
    typealias CellConfig = (UITableViewCell, T) -> ()

    let items: [T]
    let identifiers: [String]
    let cellConfig: CellConfig
    
    init(identifiers: [String], items: [T], cellConfig: @escaping CellConfig) {
        self.identifiers = identifiers
        self.items = items
        self.cellConfig = cellConfig
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: self.identifiers[indexPath.section], for: indexPath)
        let item = items[indexPath.row]
        cellConfig(cell, item)
        return cell
    }
}
