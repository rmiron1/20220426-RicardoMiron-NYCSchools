//
//  VisitTableViewCell.swift
//  PublicSchools
//
//  Created by Ricardo Miron on 4/26/22.
//

import UIKit

class VisitTableViewCell: BaseTableViewCell {
    
    static let identifier = String(describing: VisitTableViewCell.self)
    static func nib() -> UINib {
        return UINib(nibName: identifier, bundle: nil)
    }
    
    @IBOutlet weak var websiteView: LinkTextView!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    override func updateUI() {
        if let website = school?.websiteURLString {
            websiteView.text = "\(website)"
            websiteView.addLinks([website: website])
            websiteView.onLinkTap = { (urlString) in
                DispatchQueue.main.async {
                    if let url = URL(string: "\(urlString)" ) {
                        UIApplication.shared.open(url, options: [:], completionHandler: nil)
                    }
                }
                
                return true
            }
        }
    }
    
}
