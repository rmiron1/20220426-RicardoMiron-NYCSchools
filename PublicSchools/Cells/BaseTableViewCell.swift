//
//  BaseTableViewCell.swift
//  PublicSchools
//
//  Created by Ricardo Miron on 4/25/22.
//

import UIKit

class BaseTableViewCell: UITableViewCell {
    var school: School? {
        didSet {
            updateUI()
        }
    }
    
    var sats: SAT?

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func updateUI(){}
}
