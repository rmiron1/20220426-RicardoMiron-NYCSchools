//
//  ContactTableViewCell.swift
//  PublicSchools
//
//  Created by Ricardo Miron on 4/25/22.
//

import UIKit

class ContactTableViewCell: BaseTableViewCell {
    
    static let identifier = String(describing: ContactTableViewCell.self)
    static func nib() -> UINib {
        return UINib(nibName: identifier, bundle: nil)
    }
    
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var emailLabel: LinkTextView!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    override func updateUI() {
        if let email = school?.email {
            emailLabel.text = "\(email)"
            emailLabel.addLinks([email: email])
            emailLabel.onLinkTap = { uEmail in
                DispatchQueue.main.async {
                    if let url = URL(string: "mailto:\(uEmail)") {
                        UIApplication.shared.open(url, options: [:], completionHandler: nil)
                    }
                }
                return true
            }
        }
       
        addressLabel.text = school?.address
        phoneLabel.text = school?.phoneNumber
        //emailLabel.text = school?.email
    }
    
}
