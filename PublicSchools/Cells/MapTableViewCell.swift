//
//  MapTableViewCell.swift
//  PublicSchools
//
//  Created by Ricardo Miron on 4/25/22.
//

import UIKit
import MapKit

class MapTableViewCell: BaseTableViewCell {
    
    static let identifier = String(describing: MapTableViewCell.self)
    static func nib() -> UINib {
        return UINib(nibName: identifier, bundle: nil)
    }
    
    @IBOutlet weak var mapView: MKMapView!

    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    override func updateUI(){
        if let coordinates = school?.coordinates {
            setPin(location: coordinates)
        }
    }

    func setPin(location: CLLocationCoordinate2D) {
       let pin = MKPlacemark(coordinate: location)
       let coordinateRegion = MKCoordinateRegion(center: pin.coordinate, latitudinalMeters: 800, longitudinalMeters: 800)
       mapView.setRegion(coordinateRegion, animated: true)
       mapView.addAnnotation(pin)
    }
    
}
