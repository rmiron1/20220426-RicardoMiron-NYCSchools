//
//  OverviewTableViewCell.swift
//  PublicSchools
//
//  Created by Ricardo Miron on 4/25/22.
//

import UIKit

class OverviewTableViewCell: BaseTableViewCell {
    
    static let identifier = String(describing: OverviewTableViewCell.self)
    static func nib() -> UINib {
        return UINib(nibName: identifier, bundle: nil)
    }
    
    @IBOutlet weak var overviewLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    override func updateUI() {
        overviewLabel.text = school?.overview
    }
}
