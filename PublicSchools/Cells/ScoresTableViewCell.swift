//
//  ScoresTableViewCell.swift
//  PublicSchools
//
//  Created by Ricardo Miron on 4/25/22.
//

import UIKit

class ScoresTableViewCell: BaseTableViewCell {
    
    static let identifier = String(describing: ScoresTableViewCell.self)
    static func nib() -> UINib {
        return UINib(nibName: identifier, bundle: nil)
    }
    
    @IBOutlet weak var writingLabel: UILabel!
    @IBOutlet weak var readingLabel: UILabel!
    @IBOutlet weak var mathLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    override func updateUI() {
        writingLabel.text = sats?.writinAverage
        readingLabel.text = sats?.readingAverage
        mathLabel.text = sats?.mathAverage
    }
    
}
