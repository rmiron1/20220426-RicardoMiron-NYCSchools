//
//  SchoolTableViewCell.swift
//  PublicSchools
//
//  Created by Ricardo Miron on 4/19/22.
//

import UIKit

class SchoolTableViewCell: UITableViewCell {
    
    static let identifier = String(describing: SchoolTableViewCell.self)
    static func nib() -> UINib {
        return UINib(nibName: identifier, bundle: nil)
    }
    @IBOutlet weak var containerView: UIView! {
        didSet {
            containerView.layer.cornerRadius = 10
            containerView.layer.borderWidth = 0.1
            containerView.layer.borderColor = UIColor.lightGray.cgColor
            containerView.layer.shadowColor = UIColor.lightGray.cgColor
            containerView.layer.shadowOpacity = 0.5
            containerView.layer.shadowOffset = CGSize(width: 0, height: 5)
            containerView.layer.shadowRadius = 3
        }
    }
    @IBOutlet weak var topLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var bottomLabel: UILabel!
    var item: School? {
        didSet {
            topLabel?.text = item?.name
            addressLabel?.text = item?.interests
            bottomLabel?.text = item?.website
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
