//
//  SchoolViewModel.swift
//  PublicSchools
//
//  Created by Ricardo Miron on 4/19/22.
//

import Foundation

class SchoolViewModel {
    
    var updateHandler:(() -> Void)? // Closure that gets called on update.
    var onError: (() -> Void)?

    private let service: APIServiceProtocol
    private var schools: Schools? { // private due to encapsulation
        didSet {
            self.updateHandler?()
        }
    }
    
    var errorMessage: String? {
        didSet {
            self.onError?()
        }
    }
    
    init(service: APIServiceProtocol = APIService()) {
        self.service = service
    }
    
    // MARK: - Public
    public func getSchools() -> Schools? {
        if let sorted = schools?.sorted(by: <) {
            return sorted
        }
        return nil
    }
    
    public func fetchSchools() {
        service.getData(type: School.self,
                        endpoint: .schools()) { [weak self] schools, error in
            DispatchQueue.main.async {
                if let schools = schools {
                    self?.schools = schools as? Schools
                } else {
                    if error != nil {
                        self?.errorMessage = error?.rawValue
                    }
                }
            }
        }
    }
}
