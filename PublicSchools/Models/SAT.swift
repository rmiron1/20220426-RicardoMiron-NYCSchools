//
//  SAT.swift
//  PublicSchools
//
//  Created by Ricardo Miron on 4/19/22.
//

import Foundation

protocol SATsRepresentable {
    var readingAverage: String { get }
    var writinAverage: String { get }
    var mathAverage: String { get }
}

typealias SATs = [SAT]

struct SAT: Encodable, Decodable {
    let critReadAvg: String
    let mathAvg: String
    let writingAvg: String
    let school: String
    let testTakers: String
    let dbn: String
    
    enum CodingKeys: String, CodingKey {
        case critReadAvg = "sat_critical_reading_avg_score"
        case mathAvg = "sat_math_avg_score"
        case writingAvg = "sat_writing_avg_score"
        case school = "school_name"
        case testTakers = "num_of_sat_test_takers"
        case dbn
    }
}

extension SAT: SATsRepresentable {
    var readingAverage: String {
        return "Critical reading average score: " + critReadAvg
    }
    
    var writinAverage: String {
        return "Writing average score: " + writingAvg
    }
    
    var mathAverage: String {
        return "Math average score: " + mathAvg
    }
}
