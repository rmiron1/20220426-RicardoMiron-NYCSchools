//
//  School.swift
//  PublicSchools
//
//  Created by Ricardo Miron on 4/19/22.
//

import Foundation
import CoreLocation


protocol SchoolRepresentable {
    var address: String { get }
    var coordinates: CLLocationCoordinate2D? { get }
    var interests: String { get }
    var websiteURLString: String { get }
}

typealias Schools = [School]

struct School: Encodable, Decodable, Comparable {
    static func < (lhs: School, rhs: School) -> Bool {
        return lhs.name < rhs.name
    }
    
    static func > (lhs: School, rhs: School) -> Bool {
        return lhs.name > rhs.name
    }
    
    let name: String
    let website: String
    let phoneNumber: String
    let email: String?
    let totalStudents: String
    let interest1: String
    let interest2: String?
    let dbn: String
    let overview: String?
    let latitude: String?
    let longitude: String?
    let location: String
    let neighborhood: String
    let zip: String
    let state: String
    let startTime: String?
    let endTime: String?
    let city: String
    let activities: String?
    let grades: String
    
    enum CodingKeys: String, CodingKey {
        case name = "school_name"
        case website
        case phoneNumber = "phone_number"
        case email = "school_email"
        case totalStudents = "total_students"
        case interest1
        case interest2
        case dbn
        case overview = "overview_paragraph"
        case latitude
        case longitude
        case location
        case neighborhood
        case zip
        case state = "state_code"
        case startTime = "start_time"
        case endTime = "end_time"
        case city
        case activities = "extracurricular_activities"
        case grades = "finalgrades"
    }
}

extension School: SchoolRepresentable {
    var address: String {
        let lineItems = location.components(separatedBy: "(")
        return lineItems[0]
    }
    
    var coordinates: CLLocationCoordinate2D? {
        guard
            let lat = latitude,
            let lon = longitude,
            let lati = Double(lat),
            let long = Double(lon)
        else {
            return nil
        }
        return CLLocationCoordinate2D(latitude:lati, longitude:long)
    }
    
    var interests: String {
        let prefix = "Specializing in "
        if let interest2 = interest2 {
            if interest1 != interest2 {
                return prefix + interest1 + " " + interest2
            }
        }
        return prefix + interest1
    }
    
    var websiteURLString: String {
        return "https://" + website
    }

}
