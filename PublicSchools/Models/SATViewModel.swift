//
//  SATViewModel.swift
//  PublicSchools
//
//  Created by Ricardo Miron on 4/19/22.
//

import Foundation

class SATViewModel {
    var updateView:(() -> Void)?
    var onError: (() -> Void)?

    private let service: APIService
    private var sats: SATs? {
        didSet {
            self.updateView?()
        }
    }
    
    var errorMessage: String? {
        didSet {
            self.onError?()
        }
    }
    
    init(service: APIService = .init()) {
        self.service = service
    }
    
    func fetchSATData() {
        service.getData(type: SAT.self,
                        endpoint: .sats()) { [weak self] sats, error in
            DispatchQueue.main.async {
                if let sats = sats {
                    self?.sats = sats as? SATs
                } else {
                    if error != nil {
                        self?.errorMessage = error?.rawValue
                    }
                }
            }
        }
    }
    
    func getSAT(forDbn dbn: String) -> SAT? {
        if let sat = sats?.first(where: { $0.dbn == dbn}) {
            return sat
        }
        return nil
    }
}
