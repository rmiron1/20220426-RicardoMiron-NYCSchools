//
//  LinkTextView.swift
//  PublicSchools
//
//  Created by Ricardo Miron on 4/25/22.
//

import UIKit

/*
  This subclass allows text to be treated as a link.
 */
class LinkTextView: UITextView {
    
    typealias OnTap = (URL) -> Bool
    typealias Links = [String: String]
    var onLinkTap: OnTap?
    
    override init(frame: CGRect, textContainer: NSTextContainer?) {
        super.init(frame: frame, textContainer: textContainer)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    
    private func commonInit() {
        isEditable = false
        isSelectable = true
        isScrollEnabled = false
        textContainerInset = .zero
        delegate = self
    }
    
    func addLinks(_ links: Links) {
        guard attributedText.length > 0  else {
            return
        }
        let text = NSMutableAttributedString(attributedString: attributedText)
        
        for (linkText, urlString) in links {
            if linkText.count > 0 {
                let linkRange = text.mutableString.range(of: linkText)
                text.addAttribute(.link, value: urlString, range: linkRange)
            }
        }
        attributedText = text
    }
}

extension LinkTextView: UITextViewDelegate {
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange) -> Bool {
        return onLinkTap?(URL) ?? true
    }
    
    func textViewDidChangeSelection(_ textView: UITextView) {
        textView.selectedTextRange = nil
    }
}
