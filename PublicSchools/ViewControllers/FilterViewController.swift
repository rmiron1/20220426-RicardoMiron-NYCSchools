//
//  FilterViewController.swift
//  PublicSchools
//
//  Created by Ricardo Miron on 4/25/22.
//

import UIKit

class FilterTableViewController: UITableViewController {
    
    var onSelection:((String) -> Void)?
    var categories: [String]?
    private var dataSource: TableViewDataSource<String>?

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        updateDataSource()
    }
    
    func updateDataSource() {
        guard let items = categories else { return }
        dataSource = TableViewDataSource(identifiers: ["Cell"],
                                      items: items,
                                      cellConfig: { cell, item in
            cell.textLabel?.text = item
        })
        
        DispatchQueue.main.async {
            self.tableView.dataSource = self.dataSource
            self.tableView.reloadData()
        }
    }
    
    @IBAction func cancelButtonPressed() {
        dismiss(animated: true, completion: nil)
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let selection = categories?[indexPath.row] {
            onSelection?(selection)
            self.dismiss(animated: true, completion: nil)
        }
    }
}
