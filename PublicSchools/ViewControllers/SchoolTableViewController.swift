//
//  SchoolTableViewController.swift
//  PublicSchools
//
//  Created by Ricardo Miron on 4/19/22.
//

import UIKit

/*
    I prefer to use a combination of coordinator and router for navigation but given
 the time constraints i chose to use storyboard segues
 */

class SchoolTableViewController: UITableViewController {
    enum FilterType: Int {
        case none
        case name
        case interest
    }
    
    private let identifiers = [SchoolTableViewCell.identifier]
    private var dataSource: TableViewDataSource<School>?
    var viewModel: SchoolViewModel?
    
    private var schools: Schools? {
        didSet {
            filteredSchools = schools
        }
    }
    
    private var filteredSchools: Schools? {
        didSet {
            updateDataSource()
        }
    }
    
    private var search = "" {
        didSet {
            switch (filteType) {
            case .name:
                filteredSchools = search != "" ? schools?.filter {
                    $0.name.uppercased().contains(search.uppercased())
                } : schools
            case .interest:
                filteredSchools = search != "" ? schools?.filter {
                    $0.interest1.uppercased().contains(search.uppercased())
                } : schools
            default:
                filteredSchools = schools
            }
        }
    }
    private var filterCategories: [String]?
    private var selectedItem: School?
    var satViewModel: SATViewModel?
    private lazy var searchController: UISearchController = {
        let search = UISearchController(searchResultsController: nil)
        search.searchResultsUpdater = self
        search.obscuresBackgroundDuringPresentation = false
        search.hidesNavigationBarDuringPresentation = false
        self.definesPresentationContext = false
        search.searchBar.placeholder = "Search by school name"
        search.searchBar.tintColor = UIColor.black
        return search
    }()
    var filteType: FilterType = .none {
        didSet {
            filteredByCategories = filteType == .interest
        }
    }
    var filteredByCategories: Bool = false {
        didSet {
            if filteredByCategories {
                navigationItem.rightBarButtonItem?.image = UIImage(named: "unfilter")
            } else {
                navigationItem.rightBarButtonItem?.image = UIImage(named: "filter")
            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.searchController = searchController
        /* Attempted to add an image that aligned to the left of the
         navigation bar but it did not look good so opted for this instead.
         */
        let logo = UIImage(named: "nycImage")
        let imageView = UIImageView(image:logo)
        imageView.contentMode = .scaleAspectFit
        self.navigationItem.titleView = imageView
        registerCell()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        viewModel?.fetchSchools()
        viewModel?.updateHandler = { [weak self] in
            self?.schools = self?.viewModel?.getSchools()
            if let these = self?.schools {
                self?.filterCategories = these.map { $0.interest1 }.unique()
            }
        }
        
        viewModel?.onError = { [weak self] in
            self?.showError()
        }
    }
    
    private func showError()  {
        self.showAlert(title: "Attention",
                       message: "Unable to fetch the data at this time.")
    }
    
    private func filterBy(_ text: String, type: FilterType) {
        filteType = type
        search = text
    }
    
    private func registerCell() {
        tableView.register(SchoolTableViewCell.nib(), forCellReuseIdentifier: SchoolTableViewCell.identifier)
    }
    
    private func updateDataSource() {
        if let items = filteredSchools {
            dataSource = TableViewDataSource(identifiers: identifiers, items: items, cellConfig: { cell, item in
                if cell.isKind(of: SchoolTableViewCell.self) {
                    let actualCell = cell as? SchoolTableViewCell
                    actualCell?.item = item
                }
            })
            
            DispatchQueue.main.async {
                self.tableView.dataSource = self.dataSource!
                self.tableView.reloadData()
            }
        }
    }
    
    @IBAction func filterButtonPressed() {
        if filteType == .interest {
            filterBy("", type: .none)
        } else {
            performSegue(withIdentifier: "presentFilter", sender: nil)
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let items = filteredSchools{
            selectedItem = items[indexPath.row]
            performSegue(withIdentifier: "showDetails", sender: nil)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destinationVC = segue.destination as? DetailsTableViewController {
            destinationVC.school = selectedItem
            destinationVC.satViewModel = satViewModel
        }
        
        
        if let destinationVC = segue.destination as? UINavigationController {
            if let rootVC = destinationVC.viewControllers.first {
                if rootVC.isKind(of: FilterTableViewController.self) {
                    let vc = rootVC as? FilterTableViewController
                    vc?.categories = filterCategories
                    vc?.onSelection = { selection in
                        self.filterBy(selection, type: .interest)
                    }
                }
            }
        }
    }
}

extension SchoolTableViewController: UISearchResultsUpdating, UISearchBarDelegate {
    func updateSearchResults(for searchController: UISearchController) {
        if let searchText = searchController.searchBar.text {
            filterBy(searchText, type: .name)
        }
    }
        
}

extension Sequence where Iterator.Element: Hashable {
    func unique() -> [Iterator.Element] {
        var seen: Set<Iterator.Element> = []
        return filter { seen.insert($0).inserted }
    }
}
