//
//  DetailsTableViewController.swift
//  PublicSchools
//
//  Created by Ricardo Miron on 4/25/22.
//

import UIKit

class DetailsTableViewController: UITableViewController {
    private let identifiers = [OverviewTableViewCell.identifier,
                               ScoresTableViewCell.identifier,
                               VisitTableViewCell.identifier,
                               ContactTableViewCell.identifier,
                               MapTableViewCell.identifier]
    private var dataSource: TableViewDataSource<School>?
    var school: School?
    var sat: SAT? {
        didSet {
            tableView.reloadData()
        }
    }
    var satViewModel: SATViewModel?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.rowHeight = UITableView.automaticDimension
        satViewModel?.fetchSATData()
        satViewModel?.updateView = { [weak self] in
            self?.updateView()
        }
        registerCells()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.title = school?.name
    }
    
    func updateView() {
        if let item = school,
           let sat = satViewModel?.getSAT(forDbn: item.dbn) {
            self.sat = sat
        }
    }
    
    private func registerCells() {
        tableView.register(OverviewTableViewCell.nib(), forCellReuseIdentifier: OverviewTableViewCell.identifier)
        tableView.register(ScoresTableViewCell.nib(), forCellReuseIdentifier: ScoresTableViewCell.identifier)
        tableView.register(VisitTableViewCell.nib(), forCellReuseIdentifier: VisitTableViewCell.identifier)
        tableView.register(ContactTableViewCell.nib(), forCellReuseIdentifier: ContactTableViewCell.identifier)
        tableView.register(MapTableViewCell.nib(), forCellReuseIdentifier: MapTableViewCell.identifier)
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: identifiers[indexPath.row], for: indexPath)
        if cell.isKind(of: BaseTableViewCell.self) {
            let actualCell = cell as? BaseTableViewCell
            actualCell?.sats = self.sat
            actualCell?.school = self.school
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return identifiers.count
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 4:
            return UIScreen.main.bounds.width * 0.6
        default:
            return UITableView.automaticDimension
        }
    }
}
