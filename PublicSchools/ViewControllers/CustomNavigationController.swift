//
//  CustomNavigationController.swift
//  PublicSchools
//
//  Created by Ricardo Miron on 4/25/22.
//

import UIKit

/*
 Since some of the school names are long, I added this workaround to allow two lines
 in the navigation controller bar.
 */
class CustomNavigationController: UINavigationController, UINavigationBarDelegate {    
    func navigationBar(_ navigationBar: UINavigationBar, shouldPush item: UINavigationItem) -> Bool {
        item.setValue(true, forKey: "__largeTitleTwoLineMode")
        return true
    }
}
