//
//  Endpoint.swift
//  Forecast
//
//  Created by Ricardo Miron on 4/22/22.
//

import Foundation

/*
 Schools = https://data.cityofnewyork.us/resource/s3k6-pzi2.json
 Sats = https://data.cityofnewyork.us/resource/f9bf-2cp4.json
 */

struct Endpoint {
    let path: String
    let queryItems: [URLQueryItem]?
}

extension Endpoint {
    var url: URL? {
        var components = URLComponents()
        components.scheme = "https"
        components.host = "data.cityofnewyork.us"
        components.path = path
        components.queryItems = queryItems
        return components.url
    }
}

extension Endpoint {
    static func schools() -> Endpoint {
        return Endpoint(
            path: "/resource/s3k6-pzi2.json",
            queryItems: nil)
    }
    
    static func sats() -> Endpoint {
        return Endpoint(
            path: "/resource/f9bf-2cp4.json",
            queryItems: nil)
    }
    
    static private func queryItems() -> [URLQueryItem]? {
        return nil
    }
}

