//
//  APIService.swift
//  PublicSchools
//
//  Created by Ricardo Miron on 4/19/22.
//

import Foundation

typealias Completion<T> = (T?, APIError?) -> ()

protocol APIServiceProtocol {
    func getData<T: Codable>(type: T.Type, endpoint: Endpoint, completion:@escaping Completion<Any>)
}

enum APIError: String, Error {
    case noInternet = "No Internet Available"
    case decodingFailed = "Parsing issue"
    case badURL = "The url is invalid"
}
    
class APIService: APIServiceProtocol {
    private let session: URLSession
    private let decoder: JSONDecoder
    
    init(session: URLSession = .shared, decoder: JSONDecoder = .init()) {
        self.session = session
        self.decoder = decoder
    }
    
    func getData<T: Codable>(type: T.Type, endpoint: Endpoint, completion:@escaping Completion<Any>) {
        guard let url = endpoint.url else {
            completion(nil, APIError.badURL)
            return
        }
        if !Reachability.isConnectedToNetwork() {
            completion(nil, APIError.noInternet)
            return
        }
        
        session.dataTask(with: url) { [weak self] data, response, error in
            guard let self = self else { return }
            if error != nil {completion(nil, error as? APIError)} else {
                do {
                    let model = try self.decoder.decode([T].self, from: data ?? Data())
                    completion(model, nil)
                } catch {
                    print(error)
                    completion(nil, APIError.decodingFailed)
                }
            }
        }.resume()
    }
}
