//
//  PublicSchoolsTests.swift
//  PublicSchoolsTests
//
//  Created by Ricardo Miron on 4/26/22.
//

import XCTest
@testable import PublicSchools

class SchoolsViewModelTests: XCTestCase {
    
    var sut: SchoolViewModel!
    var mockAPIService: MockApiService!

    override func setUp() {
        super.setUp()
        mockAPIService = MockApiService()
        sut = SchoolViewModel(service: mockAPIService)
    }

    override func tearDown() {
        sut = nil
        mockAPIService = nil
        super.tearDown()
    }

    func testFetchSchools() {
        mockAPIService.completeSchools = Schools()
        sut.fetchSchools()
        XCTAssert(mockAPIService!.getDataCalled)
    }
    
    func testFetchSchoolsFail() {
        
        let error = APIError.decodingFailed
        sut.fetchSchools()
        mockAPIService.fetchFail(error: error )
        XCTAssertEqual( sut.errorMessage, error.rawValue )
    }
}

class MockApiService: APIServiceProtocol {
    
    var getDataCalled = false
    
    var completeSchools: Schools = Schools()
    var completeClosure: ((Schools?, APIError?) -> ())!
    
    func getData<T: Codable>(type: T.Type, endpoint: Endpoint, completion:@escaping Completion<Any>) {
        getDataCalled = true
        completeClosure = completion
        
    }
    
    func fetchSuccess() {
        completeClosure(completeSchools, nil)
    }
    
    func fetchFail(error: APIError?) {
        completeClosure(nil, error)
    }
}
